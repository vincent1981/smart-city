《Maxima工程上手指南》

一、开发环境准备
1、安装redis数据库，启动redis默认端口；
2、安装mongodb数据库，启动默认端口；
3、copy安装ZK-ENV，其中包含：eclipse4.8、eclipse workspace（统一IDE配置、git插件、ivy插件等）、jdk8、ant1.9、tomcat9以及环境的启动命令eclipse.cmd
4、双击d:/ZK-ENV/eclipse.cmd 启动eclipse
5、从云效导入git仓库maxima/demo4zk
6、eclipse导入本工程
7、配置servers 新建tomcat9配置；
8、启动tomcat9成功后 浏览器验证http://localhost:8080/maxima/
9、选中test目录，右键 run as JUnit Test查看测试运行结果；
10、第8或9步骤 任一验证成功，说明Maxima开发环境安装成功。

二、项目自动构建
1、打开命令行工具，cd d:/zk-env
2、build
3、进入本工程根目录，如 cd d:\git\demo4zk\Maxima
4、显示可用的项目构建命令，执行命令 ant
5、运行全部测试，执行命令 ant ut test-reports
6、打测试环境war包，执行命令 ant war-uat

三、java package与分层架构说明
1、com.daren.maxima.aliyun:集成阿里云各种服务SDK的utils组件，如oss对象存储，以及后续的邮件、短信、mq等；
2、com.daren.maxima.api：maxima服务端对APP开放的rest接口——springmvc controller
3、com.daren.maxima.ommon：maxima框架基础组件，如ZkUtils、Zk分页框架、Zk表单验证框架、Hibernate实体基类；
4、com.daren.maxima.entity：hibernate实体
5、com.daren.maxima.mongo：mongodb实体及其Dao
6、com.daren.maxima.repo：hibernate repository（Dao）
7、com.daren.maxima.service：业务逻辑组件
8、com.daren.maxima.util：工具类组件
9、com.daren.maxima.vm：Zk view model（页面控制器）组件
10、com.daren.maxima.vo：用于方便页面显示和参数传递的value object值对象